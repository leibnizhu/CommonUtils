package com.turingdi;

import com.turingdi.commonutils.basic.PropertiesUtils;
import com.turingdi.commonutils.encrypt.MD5Utils;
import com.turingdi.commonutils.encrypt.SHA1Utils;
import com.turingdi.commonutils.format.DateUtils;
import com.turingdi.commonutils.format.XmlUtils;
import com.turingdi.commonutils.wechat.WaterMarkParam;
import com.turingdi.commonutils.wechat.WaterMarkUtils;
import org.junit.Test;

import java.io.File;
import java.security.DigestException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by leibniz on 17-1-10.
 */
public class testUtils {
    @Test
    public void md5Test() throws DigestException {
        System.out.println(MD5Utils.getMD5("Hello，World"));
        System.out.println(SHA1Utils.SHA1("Hello，World"));
        Map<String, Object> map = new HashMap<>();
        map.put("aaa","123");
        map.put("zzz","456");
        map.put("ggg","789");
        System.out.println(map);
        System.out.println(SHA1Utils.getOrderByLexicographic(map));
    }

    @Test
    public void testDateUtils(){
        System.out.println(DateUtils.getSimpleHour(new Date())); //"16:00"——其中一种可能的返回结果
        System.out.println(DateUtils.getSimpleDate(new Date())); //"2017-01-10"——其中一种可能的返回结果
        System.out.println(DateUtils.getDate(new Date())); //"2016-01-10 16:18:34"——其中一种可能的返回结果
        System.out.println(DateUtils.getDate_day(new Date())); //"10-16"——其中一种可能的返回结果
        System.out.println(DateUtils.getHoursAgo(new Date(), 24)); //"10-16"——其中一种可能的返回结果
    }

    @Test
    public void xmlTest() throws DigestException {
        Map<String, Object> map = new HashMap<>();
        map.put("aaa","123");
        map.put("zzz","456");
        map.put("ggg","789");
        System.out.println(XmlUtils.simpleMapToXml(map));
    }

    @Test
    public void waterMarkBaseTest() {
        long startTime = System.currentTimeMillis();
        WaterMarkParam waterMarkParam = new WaterMarkParam();
        waterMarkParam.setSourceFile(new File("/usr/software/git/FissionSales/target/FissionSales/upload/invitation/azureInvitation.png"));
        waterMarkParam.setWaterMarkText("文字水印");
        waterMarkParam.setWaterMarkTextSize(30);
        waterMarkParam.setWaterMarkTextX(0);
        waterMarkParam.setWaterMarkTextY(400);
        waterMarkParam.setWaterMarkFile(new File("/home/quandong/下载/1490588825.png"));
//        waterMarkParam.setWaterMarkImage(QRCodeUtils.getImage("afsdfsadf", 200));
//        waterMarkParam.setWaterMarkFileX(0);
        waterMarkParam.setWaterMarkFileY(900);
        waterMarkParam.setTargetPath("/usr/software/git/FissionSales/target/FissionSales/upload/invitation/test");
        waterMarkParam.setTargetFileName("inv");
        waterMarkParam.setTargetType("jpg");
        System.out.println(WaterMarkUtils.waterMark(waterMarkParam));
        System.out.println(System.currentTimeMillis() - startTime);
    }

    @Test
    public void propertiesUtilsTest(){
        System.out.println(PropertiesUtils.getString("hbase", "ZKQuorum"));
        System.out.println(PropertiesUtils.getString("wechat", "appid"));
        System.out.println(PropertiesUtils.getString("alipay", "appid"));
    }
}
