package com.turingdi;

import com.turingdi.commonutils.basic.NetworkUtils;
import org.junit.Test;

import java.io.File;

/**
 * 测试网络工具类
 *
 * Created by leibniz on 2017-03-22.
 */
public class NetworkTest {
    @Test
    public void testDownloadImage(){
        String url = "http://wx.qlogo.cn/mmopen/GqIlejFTbNh8WGperMo64mf9s4ylCAsIQB7XHjP3oySibkhQ7GS0Vs8JNxeSWZ0LnYXb1USaFbF4zK4ryhnS9hg/0";
        File file = new File("/home/leibniz/1.jpg");
        NetworkUtils.downloadFile(url, file);
    }
}
