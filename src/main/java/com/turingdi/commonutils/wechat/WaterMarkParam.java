package com.turingdi.commonutils.wechat;

import java.awt.*;
import java.io.File;

/**
 * 该类是添加水印方法的参数对象；
 * Created by quandong on 17-4-19.
 */
public class WaterMarkParam {
    private File sourceFile; // 需要添加水印的模板图片文件，这个文件必须存在
    private String waterMarkText; // 需要添加的水印文字对象，与水印图片文件、水印图片Image必须至少存在一个
    private Integer waterMarkTextSize; // 水印文字的大小，可为空，空时默认为60
    private Font waterMarkTextFont; // 水印文字字体
    private Color waterMarkTextColor;//水印文字颜色
    private Integer waterMarkTextX; // 水印文字左下角的X坐标，可为空，空时默认水平居中，该值只在水印文字存在时有效
    private Integer waterMarkTextY; // 水印文字左下角的Y坐标，可为空，空时默认垂直居中，该值只在水印文字存在时有效
    private File waterMarkFile; // 需要添加的水印图片(File格式)，与水印文字对象、水印图片Image必须至少存在一个,waterMarkImage存在时该值无效
    private Image waterMarkImage; // 需要添加的水印图片(Image格式)，与水印文字对象、水印图片文件必须至少存在一个
    private Integer waterMarkFileX; // 水印图片的左上角的X坐标，可为空，空时默认水平居中，该值只在水印图片存在时有效
    private Integer waterMarkFileY; // 水印图片的左上角的Y坐标，可为空，空时默认垂直居中，该值只在水印图片存在时有效
    private String targetPath; // 目标文件的保存目录，不包含文件名，可以以“/”结尾，没有时会自动加上，是必须参数
    private String targetFileName; // 目标文件的文件名称，不包含后缀名，不能以“.”开头，是必须参数
    private String targetType; // 目标文件的类型，只能填png或jpg，其他情况均默认png格式

    public Color getWaterMarkTextColor() {
        return waterMarkTextColor;
    }

    public void setWaterMarkTextColor(Color waterMarkTextColor) {
        this.waterMarkTextColor = waterMarkTextColor;
    }

    public Font getWaterMarkTextFont() {
        return waterMarkTextFont;
    }

    public void setWaterMarkTextFont(Font waterMarkTextFont) {
        this.waterMarkTextFont = waterMarkTextFont;
    }

    public File getSourceFile() {
        return sourceFile;
    }

    public void setSourceFile(File sourceFile) {
        this.sourceFile = sourceFile;
    }

    public String getWaterMarkText() {
        return waterMarkText;
    }

    public void setWaterMarkText(String waterMarkText) {
        this.waterMarkText = waterMarkText;
    }

    public Integer getWaterMarkTextSize() {
        return waterMarkTextSize;
    }

    public void setWaterMarkTextSize(Integer waterMarkTextSize) {
        this.waterMarkTextSize = waterMarkTextSize;
    }

    public Integer getWaterMarkTextX() {
        return waterMarkTextX;
    }

    public void setWaterMarkTextX(Integer waterMarkTextX) {
        this.waterMarkTextX = waterMarkTextX;
    }

    public Integer getWaterMarkTextY() {
        return waterMarkTextY;
    }

    public void setWaterMarkTextY(Integer waterMarkTextY) {
        this.waterMarkTextY = waterMarkTextY;
    }

    public File getWaterMarkFile() {
        return waterMarkFile;
    }

    public void setWaterMarkFile(File waterMarkFile) {
        this.waterMarkFile = waterMarkFile;
    }

    public Integer getWaterMarkFileX() {
        return waterMarkFileX;
    }

    public void setWaterMarkFileX(Integer waterMarkFileX) {
        this.waterMarkFileX = waterMarkFileX;
    }

    public Integer getWaterMarkFileY() {
        return waterMarkFileY;
    }

    public void setWaterMarkFileY(Integer waterMarkFileY) {
        this.waterMarkFileY = waterMarkFileY;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public void setTargetPath(String targetPath) {
        this.targetPath = targetPath;
    }

    public String getTargetFileName() {
        return targetFileName;
    }

    public void setTargetFileName(String targetFileName) {
        this.targetFileName = targetFileName;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public Image getWaterMarkImage() {
        return waterMarkImage;
    }

    public void setWaterMarkImage(Image waterMarkImage) {
        this.waterMarkImage = waterMarkImage;
    }
}