package com.turingdi.commonutils.wechat;

import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

import static com.turingdi.commonutils.wechat.OAuthController.OAUTH_BASE_API;


/**
 * 全站通用的过滤器
 * 先判断是否例外，不需要过滤；
 * 再判断Session有没有OpenID，没有则跳到授权页；
 *
 * Created by leibniz on 2017-03-16.
 */
@Component
public abstract class OAuthFilter extends OncePerRequestFilter implements Filter {
    /**
     * 访问时不需要公众号授权的uri
     * 包括:获取微信用户信息(公众号授权时的回调地址)
     */
    private static String[] AUTH_EXCLUDED_URL;

    //我们的公众号授权回调地址,获取OpenId
    private String openIdCallback;

    /**
     * 允许子类传入不需要公众好授权的uri
     * 配合web.xml，方便配置一个Controller里面除了个别（传入的参数）方法以外都要授权
     *
     * @param oauthExcludedUrls 不需要公众号授权的uri
     * @param oauthControllerUri    授权Controller的请求地址，从“http”开始
     * @author Leibniz
     */
    protected OAuthFilter(String[] oauthExcludedUrls, String oauthControllerUri) {
        if (oauthExcludedUrls == null) {
            AUTH_EXCLUDED_URL = new String[0];
        } else {
            AUTH_EXCLUDED_URL = oauthExcludedUrls;
        }
        this.openIdCallback = oauthControllerUri + "/getOpenId?visitUrl=";
    }


    /**
     * 判断请求的活动页面是否正在进行，不是进行中的跳到错误页面；
     * 判断是否已授权，已授权则直接放行；
     * 未授权则重定向到微信授权接口
     *
     * @param request：http请求对象
     * @param response：http响应对象
     * @param filterChain：过滤链
     * @author Leibniz
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        // 请求的uri
        String uri = request.getRequestURI().replaceAll(request.getContextPath(), "");

        // 当前访问页面是否需要公众号授权
        boolean openIdFilter = needOpenIdFilter(uri);

        // 判断当前请求是否不满足条件需要重定向
        String beforeRedirect = processRedirect(request);
        if (beforeRedirect != null) {
            response.sendRedirect(beforeRedirect);
            return;
        }

        if (openIdFilter) {
            // 从session中获取OpenID
            String openId = getOpenIDFromRequest(request);
            if (null != openId && !"".equals(openId)) {
                //openID不为空,已授权，则继续
                filterChain.doFilter(request, response);
            } else {
                String appId = getAppIdFromRequest(request);
                String appSecret = getAppSecretFromRequest(request);
                String visitUrl = getVisitAfterOAuthUrl(request, uri, appId, appSecret);
                String oauthUrl = String.format(OAUTH_BASE_API, appId, URLEncoder.encode(openIdCallback + visitUrl, "UTF-8"));
                response.sendRedirect(oauthUrl);
            }
        } else {
            // 如果不需要公众号授权，则继续
            filterChain.doFilter(request, response);
        }
    }

    /**
     * 定义授权完成后，最终访问的地址。
     * 默认是访问当前请求的地址。
     * 允许子类覆盖
     *
     * @param request   Http请求路径
     * @param uri       当前请求路径URI（不包括项目名）
     * @param appId     微信AppID
     * @param appSecret 微信AppSecret
     * @return 权完成后，最终访问的地址
     *
     * @author Leibniz
     */
    protected String getVisitAfterOAuthUrl(HttpServletRequest request, String uri, String appId, String appSecret) {
        return request.getQueryString() == null ? uri : uri + "?" + request.getQueryString();
    }

    /**
     * 获取微信AppID
     * 这里提供Http请求对象，可能具体业务需求根据请求的页面（如活动）查找对应的AppID
     * 也可能具体业务是忽略请求者/请求内容，直接从数据库/配置文件读取AppID
     *
     * @param request http请求对象
     * @return 微信AppID
     *
     * @author Leibniz
     */
    protected abstract String getAppSecretFromRequest(HttpServletRequest request);

    /**
     * 获取微信AppSecret
     * 这里提供Http请求对象，可能具体业务需求根据请求的页面（如活动）查找对应的AppSecret
     * 也可能具体业务是忽略请求者/请求内容，直接从数据库/配置文件读取AppSecret
     *
     * @param request http请求对象
     * @return 微信AppID
     *
     * @author Leibniz
     */
    protected abstract String getAppIdFromRequest(HttpServletRequest request);

    /**
     * 从当前请求获取可能缓存的OpenID
     * 一般可以放在Session或者Cookie
     *
     * @param request Http请求对象
     * @return 当前请求用户的OpenID，如果为null或空字符串则进行授权
     *
     * @author Leibniz
     */
    protected abstract String getOpenIDFromRequest(HttpServletRequest request);

    /**
     * 正式处理请求前，允许对请求进行判断，是否进行重定向
     * 比如请求的活动参数有误，或当前用户没有配置公众号等
     *
     * @param request Http请求对象
     * @return 返回一个地址，用于重定向。返回null时不重定向
     *
     * @author Leibniz
     */
    protected abstract String processRedirect(HttpServletRequest request);


    /**
     * 判断当前访问页面是否需要公众号授权
     *
     * @param uri 当前该请求的URI
     * @return 当前访问页面是否需要公众号授权
     */
    private boolean needOpenIdFilter(String uri) {
        return notContainStrings(uri, AUTH_EXCLUDED_URL);
    }

    /**
     * 判断target是否不包含rule数组中任意一字符串
     *
     * @param target 待判断的字符串
     * @param rule   判断的规则(是否这些字符串之一)
     * @return target是否不包含rule数组中任意一字符串
     */
    private boolean notContainStrings(String target, String[] rule) {
        boolean result = true;
        for (String s : rule) {
            if (target.contains(s)) {
                result = false;
                break;
            }
        }
        return result;
    }
}
