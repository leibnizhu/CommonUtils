package com.turingdi.commonutils.wechat.example;

import com.turingdi.commonutils.basic.PropertiesUtils;
import com.turingdi.commonutils.wechat.OAuthFilter;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by leibniz on 2017-08-15.
 */
public class OAuthFilterExample extends OAuthFilter implements Filter {
    static final String SESSION_OPENID_KEY = "OPENID_EXAMPLE";

    /**
     * 允许子类传入不需要公众好授权的uri
     * 配合web.xml，方便配置一个Controller里面除了个别（传入的参数）方法以外都要授权
     *
     * @author Leibniz
     */
    protected OAuthFilterExample() {
        super(new String[]{"oauthExample/getOpenId", "oauthExample/getUserInfo"}, //授权回调的方法不需要再进入授权Filter
                PropertiesUtils.getString("wechat", "PROJECT_URL") + "oauthExample");
    }

    @Override
    protected String getAppSecretFromRequest(HttpServletRequest request) {
        return PropertiesUtils.getString("wechat", "appsecret");
    }

    @Override
    protected String getAppIdFromRequest(HttpServletRequest request) {
        return PropertiesUtils.getString("wechat", "appid");
    }

    @Override
    protected String getOpenIDFromRequest(HttpServletRequest request) {
        return (String) request.getSession().getAttribute(SESSION_OPENID_KEY);
    }

    @Override
    protected String processRedirect(HttpServletRequest request) {
        return null;
    }
}
