package com.turingdi.commonutils.wechat;

import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * 该类用于为图片添加水印，包括文字水印和图片水印；
 * Created by quandong on 17-4-19.
 */
public class WaterMarkUtils {
    private static final Logger LOG = Logger.getLogger(WaterMarkUtils.class);

    /**
     * 方法用于为模板图片添加文字水印和图片水印,供其他方法调用;
     * 方法首先要保证要添加水印的模板图片文件(即源文件)存在，并且水印文字对象和水印图片文件至少存在一个；
     * 然后，将源文件转换成Image对象，并通过Graphics2D画笔对象将需要添加水印的模板图片画出来；
     * 接着，调用addWaterMarkText方法，在模板图片上添加水印文字；
     * 然后，如果水印图片文件存在，则调用addWaterMarkFile方法，在模板图片上添加水印图片；
     * 最后，处理拼接好目标文件(即存放添加完水印的图片的文件)的存储路径，然后将添加完水印的图片写入到文件中，并关闭输出流；
     *
     * @param waterMarkParam 添加水印涉及的参数封装成的对象，具体参数说明可以查看WaterMarkParam的属性说明
     * @return result,添加二维码水印是否成功的标识，true表示成功，false表示失败
     * Create by quandong
     */
    public static boolean waterMark(WaterMarkParam waterMarkParam) {
        Image sourceImage; // 存放源文件转换成的Image对象
        Image waterMarkImage; // 存放水印图片文件转换成的Image对象
        OutputStream outputStream = null; // 输出流对象
        boolean result = false; // 添加水印的结果，初始化为false

        if(waterMarkParam == null) {
            LOG.debug("参数对象为空"); // 将失败信息打印到控制台，供调用者调试用
            return false;
        }
        // 保证源文件存在，并且水印文字、水印图片(File格式)、水印图片（Image格式）必须至少存在一个，才能进行下面的添加水印
        if(waterMarkParam.getSourceFile() == null
                || !waterMarkParam.getSourceFile().exists()
                || (waterMarkParam.getWaterMarkImage() == null && (waterMarkParam.getWaterMarkText() == null || waterMarkParam.getWaterMarkText().equals(""))
                    && (waterMarkParam.getWaterMarkFile() == null || !waterMarkParam.getWaterMarkFile().exists()))) { // 源文件不存在或者水印文字和水印图片同时不存在
            LOG.debug("源文件为不存在，或者水印文字和水印图片文件同时不存在"); // 将失败信息打印到控制台，供调用者调试用
            return false; // 添加水印失败，返回false
        } else { // 源文件存在，同时水印文字、水印图片(File格式)、水印图片（Image格式）至少有一个存在
            try {
                sourceImage = ImageIO.read(waterMarkParam.getSourceFile()); // 将源文件转换为Image对象，下面画图要用到

                // 判断源文件转换为Image对象是否成功
                if(sourceImage == null) { // 转换失败，可能是非图片或者图片格式不正确，建议png或jpg格式
                    LOG.debug("源文件转换为Image失败，建议jpg或png格式的图片文件"); // 将失败信息打印到控制台，供调用者调试用
                    return false; // 添加水印失败，返回false
                } else { // 转换成功
                    BufferedImage bufferedImage = new BufferedImage(sourceImage.getWidth(null), sourceImage.getHeight(null), BufferedImage.TYPE_INT_RGB); // 根据源图片大小创建图片缓冲区
                    Graphics2D graphics2D = bufferedImage.createGraphics(); // 得到画笔对象，用于画图和添加水印等
                    graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR); // 设置对线段的锯齿状边缘处理
                    graphics2D.drawImage( sourceImage.getScaledInstance(sourceImage.getWidth(null), sourceImage.getHeight(null), Image.SCALE_SMOOTH), 0, 0, null); // 绘制源图片

                    addWaterMarkText(graphics2D, sourceImage, waterMarkParam); // 调用方法添加水印文字

                    // 判断是否需要添加水印图片
                    if(waterMarkParam.getWaterMarkImage() != null || (waterMarkParam.getWaterMarkFile() != null && waterMarkParam.getWaterMarkFile().exists())) { // 需要添加水印图片
                        // 判断调用者传来的是File格式还是Image格式
                        if(waterMarkParam.getWaterMarkImage() != null) { // Image格式
                            waterMarkImage = waterMarkParam.getWaterMarkImage();
                        } else { // File格式
                            waterMarkImage = ImageIO.read(waterMarkParam.getWaterMarkFile()); // 将水印图片文件转换为Image对象
                        }

                        // 判断水印图片文件转Image对象是否成功
                        if(waterMarkImage == null) { // 转换失败，可能是非图片或者图片格式不正确，建议png或jpg格式
                            LOG.debug("水印图片文件转换为Image失败，建议jpg或png格式的图片文件"); // 将失败信息打印到控制台，供调用者调试用
                            return false; // 添加水印失败，返回false
                        } else { // 转换成功
                            addWaterMarkFile(graphics2D, sourceImage, waterMarkImage, waterMarkParam.getWaterMarkFileX(), waterMarkParam.getWaterMarkFileY()); //调用方法添加水印图片
                        }
                    }

                    graphics2D.dispose(); // 释放资源
                    // 确保目标文件保存路径、目标文件名是否存在，并且文件名不以“.”开头
                    if(waterMarkParam.getTargetPath() == null || waterMarkParam.getTargetPath().equals("") || waterMarkParam.getTargetFileName() == null || waterMarkParam.getTargetFileName().equals("") || waterMarkParam.getTargetFileName().indexOf(".") == 0) { // 目标文件保存路径、目标文件名不存在，或者文件名以“.”开头
                        LOG.debug("目标文件保存路径或目标文件名为空，或者文件名格式错误"); // 将失败信息打印到控制台，供调用者调试用
                        return false; // 添加水印失败，返回false
                    } else { // 目标文件保存路径、目标文件名存在，并且文件名不以“.”开头
                        // 判断目标文件的保存路径是否以“/”结尾
                        if(!waterMarkParam.getTargetPath().endsWith("/")) { // 不以“/”结尾
                            waterMarkParam.setTargetPath(waterMarkParam.getTargetPath() + "/"); // 在结尾处添加“/”
                        }

                        // 判断目标文件名是否包含“.”
                        if(waterMarkParam.getTargetFileName().indexOf(".") > 0) { // 目标文件名包含“.”
                            waterMarkParam.setTargetFileName(waterMarkParam.getTargetFileName().substring(0, waterMarkParam.getTargetFileName().indexOf("."))); // 去掉“.”及其后面的字符
                        }

                        // 判断调用者是否有要求目标文件的类型，一般为“jpg”和“png”，其他情况默认为“png”
                        if(waterMarkParam.getTargetType() == null || (!waterMarkParam.getTargetType().equals("jpg") && !waterMarkParam.getTargetType().equals("png"))) { // 调用者要求的类型不是“jpg”和“png”，或者没指定类型
                            File catalog = new File(waterMarkParam.getTargetPath()); // 创建目标文件保存目录对象
                            // 判断该目录是否存在
                            if(!catalog.exists()) { // 不存在
                                catalog.mkdirs(); // 创建该目录
                            }

                            File file = new File(waterMarkParam.getTargetPath() + waterMarkParam.getTargetFileName() + ".png"); // 创建目标文件的对象，默认为“png”
                            // 判断该文件是否存在
                            if(!file.exists()) { // 目标文件对象不存在
                                file.createNewFile(); // 创建该文件
                            }
                            outputStream = new FileOutputStream(waterMarkParam.getTargetPath() + waterMarkParam.getTargetFileName() + ".png"); // 获取文件输出对象
                            result = ImageIO.write(bufferedImage, "png", outputStream); // 将添加完水印的图片写入到目标文件中，默认为“png”
                        } else { // 调用者指定目标文件类型为“jpg”或“png”
                            File catalog = new File(waterMarkParam.getTargetPath()); // 创建目标文件保存目录对象
                            // 判断该目录是否存在
                            if(!catalog.exists()) { // 不存在
                                catalog.mkdirs(); // 创建该目录
                            }

                            File file = new File(waterMarkParam.getTargetPath() + waterMarkParam.getTargetFileName() + "." + waterMarkParam.getTargetType().toLowerCase()); // 创建目标文件的对象，类型为调用者指定
                            // 判断该文件是否存在
                            if(!file.exists()) { // 目标文件对象不存在
                                file.createNewFile(); // 创建该文件
                            }
                            outputStream = new FileOutputStream(waterMarkParam.getTargetPath() + waterMarkParam.getTargetFileName() + "." + waterMarkParam.getTargetType().toLowerCase()); // 获取文件输出对象
                            result = ImageIO.write(bufferedImage, waterMarkParam.getTargetType(), outputStream); // 将添加完水印的图片写入到目标文件中，类型为调用者指定的类型
                        }

                        // 判断图片写入到目标文件是否成功
                        if(result) { // 成功
                            LOG.debug("添加水印完成"); // 将成功信息打印到控制台，供调用者调试用
                        } else { // 失败
                            LOG.debug("添加水印失败"); // 将失败信息打印到控制台，供调用者调试用
                        }
                    }
                }
            } catch (IOException e) { // 捕获文件转换为Image对象时的异常
                e.printStackTrace(); // 输出异常
            } finally {
                try {
                    // 判断文件输出流是否为空
                    if (null != outputStream) { // 不为空
                        outputStream.close(); // 关闭文件输出对象
                    }
                } catch (IOException e) { // 捕获输出流关闭异常
                    e.printStackTrace(); // 输出异常
                }
            }
            return result; // 返回结果
        }
    }

    /**
     * 该方法用于在模板图片上添加水印文字；
     * 方法先判断是否需要添加水印文字；
     * 如果需要添加水印文字，则设置字体的大小和类型等，然后计算水印文字的位置；
     * 最后，将水印文字添加到模板图片上；
     *
     * @param graphics2D 画笔对象，用于在模板图片上添加水印文字
     * @param sourceImage 模板图片的Image对象
     * @param waterMark 水印对象
     * Create by quandong
     */
    private static void addWaterMarkText(Graphics2D graphics2D, Image sourceImage, WaterMarkParam waterMark) {
        String waterMarkText = waterMark.getWaterMarkText();
        Integer waterMarkTextSize = waterMark.getWaterMarkTextSize();
        Integer waterMarkTextX = waterMark.getWaterMarkTextX();
        Integer waterMarkTextY = waterMark.getWaterMarkTextY();

        // 判断是否需要添加水印文字
        if(waterMarkText != null && !waterMarkText.equals("")) { // 需要添加文字水印
            int textX; // 水印文字左下角的X坐标
            int textY; // 水印文字左下角的Y坐标
            Font font; // 水印文字的字体对象

            // 判断调用者是否需要设置水印字体的大小，不需要时默认60
            if(waterMark.getWaterMarkTextFont() != null){
                font = waterMark.getWaterMarkTextFont();
            } else if(waterMarkTextSize == null) { // 不需要设置水印字体大小，默认60
                font = new Font("微软雅黑", Font.BOLD, 60); // 设置水印文字的字体类型、粗细和大小
            } else { // 需要设置水印字体大小，以调用者的参数值为准
                font = new Font("微软雅黑", Font.BOLD, waterMarkTextSize); // 设置水印文字的字体类型、粗细和大小
            }
            graphics2D.setFont(font); // 设置画笔对象的字体
            if(waterMark.getWaterMarkTextColor() != null) {
                graphics2D.setColor(waterMark.getWaterMarkTextColor());//设置字体颜色
            }
            FontMetrics fontMetrics = graphics2D.getFontMetrics(); // 该对象用于设置水印文字水平居中

            // 判断调用者是否需要设置水印字体左下角的X坐标，不需要时默认水平居中
            if(waterMarkTextX == null) { // 不需要设置水印字体左下角的X坐标，默认水平居中
                textX = (sourceImage.getWidth(null) - fontMetrics.stringWidth(waterMarkText))/2; // 源图片宽度减水印字体宽度再除以2
            } else { // 需要设置水印字体左下角的X坐标，以调用者的参数值为准
                textX = waterMarkTextX; // 设置水印字体左下角的X坐标为调用者的参数值
            }
            // 判断调用者是否需要设置水印字体左下角的Y坐标，不需要时默认垂直居中
            if (waterMarkTextY == null) { // 不需要设置水印字体左下角的Y坐标，默认垂直居中
                textY = (sourceImage.getHeight(null) - fontMetrics.getHeight())/2; // 源图片高度减水印字体高度再除以2
            } else { // 需要设置水印字体左下角的Y坐标，以调用者的参数值为准
                textY = waterMarkTextY; // 设置水印字体左下角的Y坐标为调用者的参数值
            }
            graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // 去除水印文字锯齿
            graphics2D.drawString(waterMarkText, textX, textY); // 添加水印文字，并设置好水印文字的位置
        }
    }

    /**
     * 该方法用于添加水印图片；
     * 方法先得到水印图片，然后设置水印图片在模板图片上面的坐标，以及透明度等；
     * 最后，将水印图片添加到模板图片上；
     *
     * @param graphics2D 画笔对象，用于在模板图片上添加水印图片
     * @param sourceImage 模板图片的Image对象
     * @param waterMarkImage 水印图片的Image对象
     * @param waterMarkFileX 调用者传来的水印图片左上角的X坐标
     * @param waterMarkFileY 调用者传来的水印图片左上角的Y坐标
     * Create by quandong
     */
    private static void addWaterMarkFile(Graphics2D graphics2D, Image sourceImage, Image waterMarkImage, Integer waterMarkFileX, Integer waterMarkFileY) {
        int fileX; // 水印图片左上角的X坐标
        int fileY; // 水印图片左上角的Y坐标
        ImageIcon waterMarkImageIcon = new ImageIcon(waterMarkImage); // 将水印图片Image对象转换成ImageIcon对象
        Image waterMarkImg = waterMarkImageIcon.getImage(); // 得到水印图片
        float alpha = 1.0f; // 透明度值

        // 判断调用者是否需要设置水印图片左上角的X坐标，不需要时默认水平居中
        if(waterMarkFileX == null) { // 不需要，默认水平居中
            fileX = (sourceImage.getWidth(null) - waterMarkImage.getWidth(null))/2; // 源图片宽度减去水印图片宽度再除以2
        } else { // 需要设置水印图片左上角的X坐标，以调用者的参数值为准
            fileX = waterMarkFileX; // 设置水印图片左上角的X坐标为调用者的参数值
        }
        // 判断调用者是否需要设置水印图片左上角的Y坐标，不需要时默认垂直居中
        if(waterMarkFileY == null) { // 不需要，默认垂直居中
            fileY = (sourceImage.getHeight(null) - waterMarkImage.getHeight(null))/2; // 源图片高度减去水印图片高度再除以2
        } else { // 需要设置水印图片左上角的Y坐标，以调用者的参数值为准
            fileY = waterMarkFileY; // 设置水印图片左上角的Y坐标为调用者的参数值
        }

        graphics2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha)); // 设置透明度
        graphics2D.drawImage(waterMarkImg, fileX, fileY, null); // 添加水印图片,并设置水印图片的位置
    }
}