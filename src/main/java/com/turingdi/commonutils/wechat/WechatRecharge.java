package com.turingdi.commonutils.wechat;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.turingdi.commonutils.basic.CommonUtils;
import com.turingdi.commonutils.basic.NetworkUtils;
import com.turingdi.commonutils.basic.PropertiesUtils;
import com.turingdi.commonutils.encrypt.MD5Utils;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

@SuppressWarnings("unused")
public class WechatRecharge {
	
	/**
	 * 充值接口
	 * @author lws 2016-12-10
	 * 参数： 充值目标账号（手机号）,商户号,服务接口编号,充值产品编号,商户本地订单号,接口版本号：V100,时间戳,签名
	 * @return 返回的状态码
	 */
	public static String rechargeAccount(String accountVal, int merchant, int clientId, int product, String outTradeNo, String version, String key) throws IOException{
		Map<String, Object> map = new TreeMap<>();
		map.put("accountVal", accountVal);
		map.put("merchant", merchant);
		map.put("clientId", clientId);
		map.put("product", product);
		map.put("outTradeNo", outTradeNo);
		map.put("version", version);
		map.put("ts", System.currentTimeMillis());
		//进行map排序
		Map<String, Object> resultMap = CommonUtils.sortMapByKey(map);
		//将map转为json对象
		JSONObject jsondata = JSON.parseObject(JSON.toJSONString(map));
		//获取签名
		jsondata.put("sign", getSign(resultMap, key));
		//执行接口
	    return NetworkUtils.postRequestWithData(PropertiesUtils.getString("wechat", "recharge"), jsondata.toString(),"json");
	}
	
	/**
	 * 查询订单接口
	 * @author lws 2016-12-17
	 * 参数： 商户号,服务接口编号,商户本地订单号,接口版本号：V100,时间戳,签名
	 * @return 返回的状态码
	 */
	public static String searchOrder(int merchant, int clientId, String outTradeNo, String version, String key) throws IOException{
		Map<String, Object> map = new TreeMap<>();
		map.put("merchant", merchant);
		map.put("clientId", clientId);
		map.put("outTradeNo", outTradeNo);
		map.put("version", version);
		map.put("ts", System.currentTimeMillis());
		//进行map排序
		Map<String, Object> resultMap = CommonUtils.sortMapByKey(map);
		//将map转为json对象
		JSONObject jsondata = JSON.parseObject(JSON.toJSONString(map));
		//获取签名
		jsondata.put("sign", getSign(resultMap, key));
		//执行接口
	    return NetworkUtils.postRequestWithData(PropertiesUtils.getString("wechat", "order_search"), jsondata.toString(),"json");
	}
	
	/**
	 * 查询余额接口
	 * @author lws 2016-12-12
	 * 参数： 商户号，服务接口编号，接口版本号：V100，时间戳,签名
	 * @return 返回的状态码
	 */
	public static String searchBalance(int merchant, int clientId, String version, String key) throws IOException{
		//将值传进map里面
		Map<String, Object> map = new TreeMap<>();
		map.put("merchant", merchant);
		map.put("clientId", clientId);
		map.put("version", version);
		map.put("ts", System.currentTimeMillis());
		//进行map排序
		Map<String, Object> resultMap = CommonUtils.sortMapByKey(map);
		//将map转为json对象
		JSONObject jsondata = JSON.parseObject(JSON.toJSONString(map));
		//获取签名
		jsondata.put("sign", getSign(resultMap, key));
		//执行接口
	    return NetworkUtils.postRequestWithData(PropertiesUtils.getString("wechat", "balance_search"), jsondata.toString(),"json");
	}
	
	/**
	 * 获取签名的MD5编码
	 *
	 * @author lws 2016-12-12
	 * @param map 要签名的参数Map
	 * @return 返回签名的MD5编码
	 */
	private static String getSign(Map<String, Object> map, String key){
		StringBuilder entityBuilder = new StringBuilder();
		//遍历map，分别拿出key和value
		for (Map.Entry<String, Object> entry : map.entrySet()) {  
			entityBuilder.append(entry.getKey());
			entityBuilder.append(entry.getValue());
		}  
		entityBuilder.append(key);
		return MD5Utils.getMD5(entityBuilder.toString()).toLowerCase();
	}
}
