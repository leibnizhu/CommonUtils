package com.turingdi.commonutils.encrypt;

/**
 * 计算MurmurHash3,提供32位和64位的版本。
 * 以下摘自维基百科：
 * MurmurHash 是一种非加密型哈希函数，适用于一般的哈希检索操作。由Austin Appleby在2008年发明，并出现了多个变种，都已经发布到了公有领域(public domain)。
 * 与其它流行的哈希函数相比，对于规律性较强的key，MurmurHash的随机分布特征表现更良好。
 *
 * Created by leibniz on 2017-01-23.
 */
@SuppressWarnings("unused")
public class MurmurHash3Utils {
    public static int hash3_x86_32(String data){
        byte[] bytes = data.getBytes();
        return murmurHash3_x86_32(bytes, bytes.length, 0xE17A1465);
    }

    public static long hash3_x64_64(String data){
        byte[] bytes = data.getBytes();
        return murmurHash3_x64_64(bytes, bytes.length, 0xE17A1465);
    }

    private static int murmurHash3_x86_32(byte[] data, int len, int seed) {
        final int nblocks = len / 4;

        int h1 = seed;

        int c1 = 0xcc9e2d51;
        int c2 = 0x1b873593;

        //----------
        // body

        for (int i = 0; i < nblocks; i++) {
            int k1 = getBlock32(data, 4 * i);

            k1 *= c1;
            k1 = (k1 << 15) | (k1 >>> (32 - 15));
            k1 *= c2;

            h1 ^= k1;
            h1 = (h1 << 13) | (h1 >>> (32 - 13));
            h1 = h1 * 5 + 0xe6546b64;
        }

        //----------
        // tail

        int tail = 4 * nblocks;

        int k1 = 0;

        switch (len & 3) {
            case 3:
                k1 ^= unsignedByte(data[tail + 2]) << 16;
            case 2:
                k1 ^= unsignedByte(data[tail + 1]) << 8;
            case 1:
                k1 ^= unsignedByte(data[tail]);
                k1 *= c1;
                k1 = (k1 << 15) | (k1 >>> (32 - 15));
                k1 *= c2;
                h1 ^= k1;
        }

        //----------
        // finalization

        h1 ^= len;

        int h = h1;
        h ^= h >>> 16;
        h *= 0x85ebca6b;
        h ^= h >>> 13;
        h *= 0xc2b2ae35;
        h ^= h >>> 16;
        h1 = h;

        return h1;
    }

    private static int getBlock32(byte[] data, int i) {
        return unsignedByte(data[i]) |
                unsignedByte(data[i + 1]) << 8 |
                unsignedByte(data[i + 2]) << 16 |
                unsignedByte(data[i + 3]) << 24;
    }

    private static int unsignedByte(byte b) {
        return ((int) b) & 0xFF;
    }

    private static long getBlock64(byte[] data, int i) {
        long low = ((long) getBlock32(data, 8 * i)) & 0xFFFFFFFFL;
        long high = ((long) getBlock32(data, 8 * i + 4)) & 0xFFFFFFFFL;
        return low | high << 32;
    }

    private static long fmix64(long k) {
        k ^= k >>> 33;
        k *= 0xff51afd7ed558ccdL;
        k ^= k >>> 33;
        k *= 0xc4ceb9fe1a85ec53L;
        k ^= k >>> 33;

        return k;
    }

    private static long murmurHash3_x64_64(byte[] data, int len, int seed) {
        final int nblocks = len / 16;

        long h1 = ((long) seed) & 0xFFFFFFFFL;
        long h2 = h1;

        final long c1 = 0x87c37b91114253d5L;
        final long c2 = 0x4cf5ad432745937fL;

        //----------
        // body

        for (int i = 0; i < nblocks; i++) {
            long k1 = getBlock64(data, 2 * i);
            long k2 = getBlock64(data, 2 * i + 1);

            k1 *= c1;
            k1 = ROTL64(k1, 31);
            k1 *= c2;
            h1 ^= k1;

            h1 = ROTL64(h1, 27);
            h1 += h2;
            h1 = h1 * 5 + 0x52dce729L;

            k2 *= c2;
            k2 = ROTL64(k2, 33);
            k2 *= c1;
            h2 ^= k2;

            h2 = ROTL64(h2, 31);
            h2 += h1;
            h2 = h2 * 5 + 0x38495ab5L;
        }

        //----------
        // tail

        int tail = 16 * nblocks;

        long k1 = 0;
        long k2 = 0;

        switch (len & 15) {
            case 15:
                k2 ^= (long) (unsignedByte(data[tail + 14])) << 48;
            case 14:
                k2 ^= (long) (unsignedByte(data[tail + 13])) << 40;
            case 13:
                k2 ^= (long) (unsignedByte(data[tail + 12])) << 32;
            case 12:
                k2 ^= (long) (unsignedByte(data[tail + 11])) << 24;
            case 11:
                k2 ^= (long) (unsignedByte(data[tail + 10])) << 16;
            case 10:
                k2 ^= (long) (unsignedByte(data[tail + 9])) << 8;
            case 9:
                k2 ^= (long) (unsignedByte(data[tail + 8]));
                k2 *= c2;
                k2 = ROTL64(k2, 33);
                k2 *= c1;
                h2 ^= k2;

            case 8:
                k1 ^= (long) (unsignedByte(data[tail + 7])) << 56;
            case 7:
                k1 ^= (long) (unsignedByte(data[tail + 6])) << 48;
            case 6:
                k1 ^= (long) (unsignedByte(data[tail + 5])) << 40;
            case 5:
                k1 ^= (long) (unsignedByte(data[tail + 4])) << 32;
            case 4:
                k1 ^= (long) (unsignedByte(data[tail + 3])) << 24;
            case 3:
                k1 ^= (long) (unsignedByte(data[tail + 2])) << 16;
            case 2:
                k1 ^= (long) (unsignedByte(data[tail + 1])) << 8;
            case 1:
                k1 ^= (long) (unsignedByte(data[tail + 0]));
                k1 *= c1;
                k1 = ROTL64(k1, 31);
                k1 *= c2;
                h1 ^= k1;
        }

        //----------
        // finalization

        h1 ^= len;
        h2 ^= len;

        h1 += h2;
        h2 += h1;

        h1 = fmix64(h1);
        h2 = fmix64(h2);

        h1 += h2;
        h2 += h1;

        return h1;
    }

    private static long ROTL64(long x, int r) {
        return (x << r) | (x >>> (64 - r));
    }
}
