package com.turingdi.commonutils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@SuppressWarnings("unused")
public class BaseController {
	private final static String HTML_CHARSET_UTF8 = "text/html; charset=UTF-8";
	private final static String XML_CHARSET_UTF8 = "application/xml; charset=utf-8";
	protected int iDisplayStart = 0; // 起始索引
	protected int iDisplayLength = 0; // 每页显示的行数

	// 日志
	private static Logger LOG = Logger.getLogger(BaseController.class);

	/**
	 * 获取JSON返回结果
	 *
	 * @param success 是否成功
	 * @param msg 信息
	 * @param obj 对应响应的对象
	 * @return JSON字符串
	 */
	protected String getJSONResult(boolean success, String msg, Object obj) {
		JSONObject result = new JSONObject();
		result.put(success ? "success" : "failure", true);
		result.put("msg", msg);
		result.put("obj", obj);
		return result.toString();
	}

	/**
	 *设置分页的超始行号和显示行数
	 *
	 * @author lws,leibniz
	 * @param jsonList json字符串
	 * @return sEcho(datatable指定的参数)
	 */
	protected String pageSet(String jsonList){
		JSONArray jsonArray = JSON.parseArray(jsonList);
		String sEcho = null;
		for(Object element : jsonArray){
			JSONObject jsonEle = (JSONObject)element;
            switch (jsonEle.getString("name")) {
                case "sEcho":
                    sEcho = jsonEle.getString("value");
                    break;
                case "iDisplayStart":
                    iDisplayStart = jsonEle.getInteger("value");
                    break;
                case "iDisplayLength":
                    iDisplayLength = jsonEle.getInteger("value");
                    break;
            }
		}
		return sEcho;
	}

	protected void printXml(HttpServletResponse response, String str) throws IOException {
		LOG.debug(str);
		printWrite(response, str, XML_CHARSET_UTF8);
	}

	protected void printStr(HttpServletResponse response, String str) throws IOException {
		LOG.debug(str);
		printWrite(response, str, HTML_CHARSET_UTF8);
	}

	private void printWrite(HttpServletResponse response, String str, String contentType) throws IOException {
		response.setHeader("Cache-Control", "no-cache");
		response.setContentType(contentType);
		PrintWriter writer = response.getWriter();
		writer.print(str);
		writer.close();
	}

/*	/**
	 * 获取会话中的用户信息
	 * @param request
	 * @return
	 *//*
	protected SessionUser getSessionUser(HttpServletRequest request) {
		return (SessionUser) request.getSession().getAttribute(SessionUser.USER);
	}

	protected int getSessionUserId(HttpServletRequest request){
		int id = 0;
		SessionUser su = getSessionUser(request);
		if(su != null){
			id = su.getId();
		}
		return id;
	}

	protected int getSessionParentId(HttpServletRequest request){
		int superior = 0;
		SessionUser su = getSessionUser(request);
		if(su != null){
			superior = su.getSuperior();
		}
		return superior;
	}
	protected int getSessionRoleTyle(HttpServletRequest request){
		int role = 0;
		SessionUser su = getSessionUser(request);
		if(su != null){
			role = su.getRole();
		}
		return role;
	}
	protected String getSessionUserName(HttpServletRequest request){
		String userName = "";
		SessionUser su = getSessionUser(request);
		if(su != null){
			userName = su.getName();
		}
		return userName;
	}*/
}
