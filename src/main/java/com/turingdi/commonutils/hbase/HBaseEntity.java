package com.turingdi.commonutils.hbase;

import java.util.Map;

/**
 * HBase表对应的实体类的抽象接口
 *
 * Created by leibniz on 16-12-21.
 */
public interface HBaseEntity {
    Map<String,Map<String,String>> toMap();

    String getRowKey();
}
