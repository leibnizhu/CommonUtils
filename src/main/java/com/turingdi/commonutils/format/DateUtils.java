package com.turingdi.commonutils.format;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@SuppressWarnings("unused")
public class DateUtils {
    private static final SimpleDateFormat H_SDF = new SimpleDateFormat("HH:00");
    private static final SimpleDateFormat YMD_SDF = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat YMDHMS_SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat DH_SDF = new SimpleDateFormat("dd-HH");

    //获取小时
    public static String getSimpleHour(Date date) {
        return H_SDF.format(date);
    }

    /**
     * 获取日期
     */
    public static String getSimpleDate(Date date) {
        return YMD_SDF.format(date);
    }

    /**
     * 获取完整日期
     */
    public static String getDate(Date date) {
        return YMDHMS_SDF.format(date);
    }

    /**
     * 获取完整日期
     */
    public static String getDate_day(Date date) {
        return DH_SDF.format(date);
    }

    /**
     * 获取前num个小时
     */
    public static String getHoursAgo(Date date, int num) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR_OF_DAY, -num);
        return YMDHMS_SDF.format(cal.getTime());
    }
}
