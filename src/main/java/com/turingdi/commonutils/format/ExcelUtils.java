package com.turingdi.commonutils.format;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.write.*;

import java.io.OutputStream;

/**
 * 功能：用于数据导出Excel文件
 *
 * @author bin
 */
@SuppressWarnings("unused")
public class ExcelUtils {
    /***
     *
     * @param title 表单名
     * @param columnName  列名
     * @param data 数据
     * @param reportDate 所查询的日期范围
     */
    public static void createXls(OutputStream fos, String title, String[] columnName, Object[][] data, String reportDate) {
        if (data.length == 0) {
            System.out.println("无数据");
        } else {
            try {
                // 创建Excel工作薄
                WritableWorkbook wwb;
                wwb = Workbook.createWorkbook(fos);
                // 添加第一个工作表并设置第一个Sheet的名字
                WritableSheet sheet = wwb.createSheet(title, 0);

                // 合并第一列第一行到第六列第一行的所有单元格
                sheet.mergeCells(0, 0, columnName.length - 1, 1);
                if (reportDate != null) {
                    sheet.mergeCells(0, 2, columnName.length - 1, 2);
                    sheet.mergeCells(0, 3, columnName.length - 1, 3);
                }
                Label label;
                WritableCellFormat wc = new WritableCellFormat();
                WritableCellFormat word = new WritableCellFormat();
                WritableCellFormat date = new WritableCellFormat(new WritableFont(WritableFont.createFont("隶书"), 15));
                WritableCellFormat origin = new WritableCellFormat(new WritableFont(WritableFont.createFont("隶书"), 15));

                WritableFont wf = new WritableFont(WritableFont.createFont("隶书"), 20);
                WritableCellFormat t = new WritableCellFormat(wf);//报表标题，格式化字体

                // 设置居中
                wc.setAlignment(Alignment.CENTRE);
                word.setAlignment(Alignment.CENTRE);
                t.setAlignment(Alignment.CENTRE);
                date.setAlignment(Alignment.RIGHT);
                origin.setAlignment(Alignment.RIGHT);
                // 设置边框线
                wc.setBorder(Border.ALL, BorderLineStyle.THIN);
                // 设置单元格的背景颜色
                wc.setBackground(jxl.format.Colour.SKY_BLUE);

                sheet.addCell(new Label(0, 0, title, t));
                if (reportDate != null) {
                    sheet.addCell(new Label(0, 2, "来源：图灵精准营销平台", origin));
                    sheet.addCell(new Label(0, 3, "数据日期：" + reportDate, date));
                }
                for (int i = 0; i < columnName.length; i++) {
                    // 在Label对象的子对象中指明单元格的位置和内容
                    label = new Label(i, 4, columnName[i], wc);
                    // 将定义好的单元格添加到工作表中
                    sheet.addCell(label);
                }
                //数据添加到单元格中并把单元格加载到工作表中
                for (int i = 0; i < data.length; i++) {
                    for (int j = 0; j < data[i].length; j++) {
                        label = new Label(j, i + 5, data[i][j].toString(), word);
                        sheet.addCell(label);

                    }
                }

                // 写入数据
                wwb.write();
                // 关闭文件
                wwb.close();
            } catch (Exception e) {
                //  System.out.println("---出现异常---");
                e.printStackTrace();
            }
        }
    }
}
