package com.turingdi.commonutils.basic;

import java.io.*;

/*
 * Created by leibniz on 16-12-23.
 */
@SuppressWarnings("unused")
public class FileUtils {

    /**
     * 从指定文件中读取String
     *
     * @param Path 文件路径
     * @author lws 2016-12-18
     */
    public static String ReadFile(String Path) {
        BufferedReader reader = null;
        StringBuilder buff = new StringBuilder();
        try {
            FileInputStream fileInputStream = new FileInputStream(Path);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, "utf-8");
            reader = new BufferedReader(inputStreamReader);
            String tempString;
            while ((tempString = reader.readLine()) != null) {
                buff.append(tempString);
            }
            reader.close();
            inputStreamReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return buff.toString();
    }

    /**
     * 将String写入指定文件
     *
     * @param Path 文件路径
     * @author lws
     */
    public static void writeFile(String Path, String data) {
        BufferedWriter writer = null;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(Path, false);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    fileOutputStream, "utf-8");
            writer = new BufferedWriter(outputStreamWriter);
            writer.write(data);
            writer.flush();
            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
