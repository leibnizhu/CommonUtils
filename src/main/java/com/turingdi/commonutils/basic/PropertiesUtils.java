package com.turingdi.commonutils.basic;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 读取配置文件，对外提供获取配置属性值的接口
 * Created by leibniz on 16-12-21.
 */
@SuppressWarnings("unused")
public class PropertiesUtils {
    private static Map<String, Properties> propsMap = new HashMap<>();
    private static Logger LOG = LoggerFactory.getLogger(PropertiesUtils.class);

    /**
     * 根据key在指定的配置文件中拿String
     *
     * @param filename 配置文件名，无需后缀
     * @param key      key
     * @return 定的配置文件中指定key对应的值
     */
    public static String getString(String filename, String key) {
        checkInited(filename);
        return propsMap.get(filename).getProperty(key);
    }

    /**
     * 根据key在指定的配置文件中拿int
     *
     * @param filename 配置文件名，无需后缀
     * @param key      key
     * @return 定的配置文件中指定key对应的值
     */
    public static int getInt(String filename, String key) {
        checkInited(filename);
        return Integer.parseInt(getString(filename, key));
    }

    /**
     * 根据key在指定的配置文件中拿double
     *
     * @param filename 配置文件名，无需后缀
     * @param key      key
     * @return 定的配置文件中指定key对应的值
     */
    public static double getDouble(String filename, String key) {
        checkInited(filename);
        return Double.parseDouble(getString(filename, key));
    }

    /**
     * 初始化
     */
    public static void init() {
        refresh();
    }

    private static void checkInited(String filename) {
        if (propsMap.get(filename) == null) {
            refresh();
            try {
                if (propsMap.get(filename) == null) {
                    throw new RuntimeException("找不到指定的文件：" + filename + ".properties");
                }
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 更新加载classpath下面所有properties文件以及config.json文件
     * 后者的同名配置会覆盖前者
     */
    private static void refresh() {
        loadPropertiesFiles();
        loadConfigJsonFile();
        LOG.debug("已读取配置文件:" + propsMap);
    }

    /**
     * 更新加载classpath下面所有properties文件
     */
    private static void loadPropertiesFiles() {
        String rootPath = PropertiesUtils.class.getResource("/").toString().replace("file:", "").replaceAll("%20", " ");
        LOG.debug("======={}=======", rootPath);
        File[] rootFiles = new File(rootPath).listFiles();
        if (null != rootFiles && rootFiles.length > 0) {
            Arrays.stream(rootFiles).filter(file -> file.getName().endsWith(".properties")).forEach(file -> {
                try {
                    Properties prop = new Properties();
                    prop.load(new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8")));
                    String key = file.getName().substring(0, file.getName().indexOf("."));
                    propsMap.put(key, prop);
                } catch (IOException e) {
                    LOG.error("读取配置文件" + file.getName() + "时抛出IO异常");
                }
            });
        }
    }

    /**
     * 更新加载classpath下面config.json文件
     * 格式：
     * {
     * 主配置名1：{
     * 子配置1:值,
     * 子配置2:值,
     * 子配置3:值,
     * },
     * 主配置名2：{
     * 子配置1:值,
     * 子配置2:值,
     * 子配置3:值,
     * }
     * }
     * 主配置名与properties文件重名的，若其中子配置名也重名，则json的值会覆盖properties文件的配置
     */
    private static void loadConfigJsonFile() {
        InputStream is = null;
        URL configJsonFileURL = PropertiesUtils.class.getResource("/config.json");
        if (configJsonFileURL == null) {
            return;
        }
        String configJsonFileName = configJsonFileURL.toString().replace("file:", "").replaceAll("%20", " ");
        String configJsonString = FileUtils.ReadFile(configJsonFileName);
        if (!CommonUtils.notEmptyString(configJsonString)) {
            return;
        }

        JSONObject json = JSONObject.parseObject(configJsonString, Feature.AllowComment);

        for (String key : json.keySet()) {
            LOG.debug("key={},value={}", key, json.get(key));
            Properties tempProp = new Properties();
            JSONObject value = (JSONObject) json.get(key);
            for (String subKey : value.keySet()) {
                tempProp.setProperty(subKey, (String) value.get(subKey));
            }
            propsMap.put(key, tempProp);
        }

        if (null != is) try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
